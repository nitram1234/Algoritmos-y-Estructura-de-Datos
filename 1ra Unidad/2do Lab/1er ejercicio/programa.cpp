#include <iostream>
#include "Pila.h"
using namespace std;

int main(){
    int tamano;
    cout << "Ingrese un tamaño para su Pila: ";
    cin >> tamano;

    Pila pila = Pila(tamano);
    char res;
    int valor;
    cout << "Cantidad máxima = " << tamano << "\n";

    while(res != '0'){

        cout << "----------------\n";
        cout << "Agregar/push[1]\nRemover/pop[2]\nVer Pila[3]\nSalir[0]\n";
        cout << "----------------\n";
        cin >> res;
        if(res == '1'){
            cout << "Ingrese valor: ";
            cin >> valor;
            pila.Push(valor);
        }
        if(res == '2'){
            pila.Pop();
        }
        if(res == '3'){
            pila.get_Pila();
        }
    }
}
