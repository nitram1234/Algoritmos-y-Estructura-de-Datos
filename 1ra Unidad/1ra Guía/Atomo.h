#include <iostream>
#include "Coordenada.h"
using namespace std;

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
    private:
        string nombre;
        int numero;
        Coordenada coordenada;

    public:
        /* constructores */
        Atomo ();
        Atomo (string nombre, int numero, Coordenada coordenada);
        
        /* métodos get */
        string get_nombre();
        int get_numero();
        Coordenada get_coordenada();

        /* métodos set */
        void set_nombre(string nombre);
        void set_numero(int numero);
        void set_coordenada(Coordenada coordenada);
};
#endif
