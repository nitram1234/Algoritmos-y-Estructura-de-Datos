#include <iostream>
#include "Coordenada.h"
#include "Atomo.h"
using namespace std;
///////////////////////////////////////////////////////////////////////
/* Constructores */
Atomo::Atomo() {
    string nombre;
    int numero;
    Coordenada coordenada;
}
Atomo::Atomo (string nombre, int numero, Coordenada coordenada){
    this->nombre = nombre;
    this->numero = numero;
    this->coordenada = coordenada;
}
///////////////////////////////////////////////////////////////////////
/* métodos get */
string Atomo::get_nombre(){
    return this->nombre;
}
int Atomo::get_numero(){
    return this->numero;
}
Coordenada Atomo::get_coordenada(){
    return this->coordenada;
}
///////////////////////////////////////////////////////////////////////
/* métodos set */
void Atomo::set_nombre(string nombre){
    this->nombre = nombre;
}
void Atomo::set_numero(int numero){
    this->numero = numero;
}
void Atomo::set_coordenada(Coordenada coordenada){
    this->coordenada = coordenada;
}
