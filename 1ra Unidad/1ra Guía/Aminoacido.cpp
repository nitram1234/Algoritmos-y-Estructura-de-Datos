#include <iostream>
#include "Atomo.h"
#include "Aminoacido.h"
#include <list>
using namespace std;
///////////////////////////////////////////////////////////////////////
/* Constructores */
Aminoacido::Aminoacido() {
    string nombre;
    int numero;
    list<Atomo> atomos;
}
Aminoacido::Aminoacido (string nombre, int numero, list<Atomo> atomos){
    this->nombre = nombre;
    this->numero = numero;
    this->atomos = atomos;
}
///////////////////////////////////////////////////////////////////////
/* métodos get */
string Aminoacido::get_nombre(){
    return this->nombre;
}
int Aminoacido::get_numero(){
    return this->numero;
}
list<Atomo> Aminoacido::get_atomos(){
    return this->atomos;
}
///////////////////////////////////////////////////////////////////////
/* métodos get */
void Aminoacido::set_nombre(string nombre){
    this->nombre = nombre;
}
void Aminoacido::set_numero(int numero){
    this->numero = numero;
}
void Aminoacido::set_atomos(list<Atomo> atomos){
    this->atomos = atomos;
}
