#include <iostream>
#include "Pila.h"
#include "Contenedor.h"
using namespace std;
Pila::Pila(){
    Contenedor* pila;
    int tope;
    bool band;
    int max;
}
Pila::Pila(int max){
        this->max = max;
        this->pila = new Contenedor[this->max];
        this->tope = 0;
        this->band = true;
    }
void Pila::PilaVacia(){
    if(this->tope == 0){
        this->band = true;
    }
    else{
        this->band = false;
    }
}
void Pila::PilaLlena(){
    if(this->tope == this->max){
        this->band = true;
    }
    else{
        this->band = false;
    }
}
void Pila::Push(Contenedor dato){
    PilaLlena();
    if(this->band == true){
        cout << "Desbordamiento, pila llena\n";
    }
    else{
        this->pila[this->tope] = dato;
        this->tope = this->tope + 1;
    }
}
Contenedor Pila::Pop(){
    PilaVacia();
    if(this->band == 1){
        cout << "Subdesbordamiento, Pila vacía\n";
    }
    else{
        Contenedor dato = this->pila[this->tope-1];
        this->tope--;
        return dato;
    }
}
void Pila::getPila(){
    for(int i = 0; i<this->tope; i++){
        cout << this->pila[i].getempresa() << "-" << this->pila[i].getNumero()<< " ";
    }
    cout << "\n";
}
int Pila::getTope(){
    return this->tope;
}
int Pila::getMax(){
    return this->max;
}
bool Pila::getBand(){
    return this->band;
}