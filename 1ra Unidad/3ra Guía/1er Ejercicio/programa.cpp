#include <iostream>
#include "Lista.h"
using namespace std;
class Controlador {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Controlador() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};
int main (void) {
    Controlador e = Controlador();
    Lista *lista = e.get_lista();
    string res; /*Se define la variable respuesta que permite controlar cuando cerrar el programa.*/ 
    do{
        cout << "Ingrese un número (f para finalizar)" << endl;

        cin >> res;
        try{
            /* Si la respuesta es f se cierra el programa...*/
            if(res == "f"){
                break;
            }
            /*... En caso contrario la variable es convertida a numero para set ingresada en la lista.*/
            int numero = stoi(res);
            lista->crear(numero);
            lista->imprimir();
        }
        catch(int e){
            cout << "Error, no es un número entero" << endl;
        }
        
    }while(1);
    return 0;
}