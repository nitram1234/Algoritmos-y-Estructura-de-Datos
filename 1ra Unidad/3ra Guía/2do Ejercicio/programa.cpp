#include <iostream>
#include "Lista.h"
using namespace std;
class Controlador {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Controlador() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};
int main (void) {

    /*Se crean las 2 listas con sus respectivos controladores.*/
    Controlador uno = Controlador();
    Lista *lista1 = uno.get_lista();
    Controlador dos = Controlador();
    Lista *lista2 = dos.get_lista();
    string res;
    do{
        /*LA lista 1 se llena hasta que la respuesta (el valor de res) sea f.*/
        cout << "Lista 1: Ingrese un número (f para finalizar y llenar la siguiente lista)" << endl;
        cin >> res;
        try{
            if(res == "f"){
                break;
            }
            /*El valor de res, si no es f, se convierte a entero y es ingresado a la lista.*/
            int numero = stoi(res);
            lista1->crear(numero);
        }
        catch(int e){
            cout << "Error, no es un número entero" << endl;
        }
        /*Apenas se rompe el ciclo de llenado de la lista 1 se empieza a llenar la lista 2.*/
    }while(1);
    do{
        cout << "Lista 2: Ingrese un número (f para finalizar y obtener resultados)" << endl;
        cin >> res;
        try{
            if(res == "f"){
                break;
            }
            /*El valor de res, si no es f, se convierte a entero y es ingresado a la lista.*/
            int numero = stoi(res);
            lista2->crear(numero);
        }
        catch(int e){
            cout << "Error, no es un número entero" << endl;
        }
    }while(1);
    /*Al terminar de llenar la lista 2 se imprimen las listas 1, 2 y la combinacion de ambas.*/
    cout << "Lista 1:" << endl;
    lista1->imprimir();
    cout << "Lista 2:" << endl;
    lista2->imprimir();
    cout << "Fusionadas:" << endl;
    lista1->fusionar(lista2);
    lista1->imprimir();
    return 0;
}