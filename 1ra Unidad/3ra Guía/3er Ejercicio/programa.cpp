#include <iostream>
#include "Lista.h"
using namespace std;
class Controlador {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Controlador() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};
int main (void) {

    /*Se crea una lista con su controlador.*/ 
    Controlador uno = Controlador();
    Lista *lista1 = uno.get_lista();
    string res;
    do{
        cout << "Ingrese un número (f para finalizar)" << endl;
        cin >> res;
        try{
            if(res == "f"){
                break;
            }
            /*Se ingresan respuestas hasta que se ingresa f para terminar el ciclo.*/
            int numero = stoi(res);
            if(lista1->getUltimo()!=NULL){
                if(lista1->getUltimo()->numero > numero){ /*Se evalúa que el numero ingresado sea mayor o igual que el numero ingresado anteriormente.*/ 
                    cout << "Por favor ingresar números crecientes [" << numero << " <= " <<lista1->getUltimo()->numero << "]"<< endl;
                }/*En caso de ser un numero menor que el anterior no se ingresa.*/
                else{ /*En caso contrario sí se ingresa el valor a la lista.*/
                    lista1->crear(numero);
                }
            }
            else{
                lista1->crear(numero);
            }
        }
        catch(int e){
            cout << "Error, no es un número entero" << endl;
        }
    }
    while(1);
    cout << "Lista:" << endl;
    lista1->rellenar();
    lista1->imprimir();
    return 0;
}