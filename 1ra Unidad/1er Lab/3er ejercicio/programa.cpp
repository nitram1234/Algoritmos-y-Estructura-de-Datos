#include <iostream>
#include "Cliente.h"
using namespace std;

int main(){
    /* Se instancian las variables que serán los atributos del Cliente... */
    string nombre, telefono;
    int saldo, tamano, posicion = 0; /* ... y la posicion 0 en la cual se empiezan a ingresar los Clientes en el arreglo. */
    bool morosidad;

    cout << "\n Ingrese la cantidad de Clientes que desea ingresar: ";
    cin >> tamano; /* Se le pide al usuario que defina el tamaño del arreglo de Clientes. */
    Cliente* arreglo_clientes = new Cliente[tamano]; /* Se crea el arreglo de Clientes. */

    for(int i=0; i < tamano; i++){ /* se inicia un for para crear los Clientes y llenar el Arreglo */
        cout << "\n Ingrese el Nombre del Cliente: ";
        cin >> nombre;

        cout << "\n Ingrese el Telefono del Cliente: ";
        cin >> telefono;

        cout << "\n Ingrese el Saldo del Cliente: ";
        cin >> saldo;

        cout << "\n Ingrese el Estado de Morosidad del Cliente(1 para moroso o 0 para no moroso): ";
        cin >> morosidad;

        Cliente cliente = Cliente(nombre, telefono, saldo, morosidad);

        arreglo_clientes[posicion] = cliente; /* Se llena el areglo. */
        posicion = posicion+1; /* La posicion cambia por cada cliente ingresado para que cada uno tenga su indice correcto en el arreglo. */

        cout << "\n Su Cliente ha sido ingresado exitosamente. \n";
    }

    int modo;
    cout << "\n Para revisar sus Clientes, ingrese cualquier entero menos '-1': \n";
    cin >> modo;

    while(modo!=-1){ /* Se abre el ciclo que le permite al usuario ver tantas veces como desea los datos del Cliente que desee.*/
        cout << "\n Ingrese el indice del Cliente que quiere revisar \n";
        cout << " Para detener el programa ingrese '-1': \n"; /* Se utiliza la misma variable del ciclo para seleccionar al Cliente, de esta forma el ciclo se puede cerrar cuando el usuario desee. */
        
        /* Se abre el ciclo que muestra la seleccion de Clientes ingresados junto a su indice, de esta forma el usuario puede ver explicitamente que Cliente ver. */
        for(int i=0; i < tamano; i++){
            cout << i << ".-" << arreglo_clientes[i].get_nombre() << "\n";            
        }

        /* Se imprimen los datos del cliente seleccionado con la variable 'modo'. */
        cin >> modo;
        cout << "\n Estos son los datos del Cliente seleccionado: " << "(" << arreglo_clientes[modo].get_nombre() << ") \n";
        cout << "\n Teléfono: " << arreglo_clientes[modo].get_telefono();
        cout << "\n Saldo: " << arreglo_clientes[modo].get_saldo();
        cout << "\n Morosidad: " << arreglo_clientes[modo].get_morosidad() << "\n";



        /*cout << "Si desea actualizar un dato del Cliente seleccionado, ingrese ''"; */
    }

    

    /*cout << cliente.get_nombre() << "\n";*/

}
