#include <fstream>
#include <iostream>
using namespace std;
#define TRUE 1
#define FALSE 0
#include "Grafo.h"
//Función de creación del nodo. 
Nodo *crearNodo(string dato) {
    Nodo *q; //Se crea el puntero nodo
    q = new Nodo(); 
    q->izq = nullptr; //No apunta a nada por ambos lados
    q->der = nullptr;
    q->info = dato;//Dato entregado como parametro
    q->FE = 0;//Factor = 0
    return q;
}
void inorden(Nodo *p){//Función que imprime los valores del arbol en inorden (orden ascendente)
    if(p != NULL){
        if(p->izq != NULL){//Imprime los valores a la izquierda del centro
            inorden(p->izq);   
        }
        cout << p->info << " ";
        if(p->der != NULL){//Imprime los valores a la derecha del centro. 
            inorden(p->der);
        }
    }
}

void InsercionBalanceado(Nodo **nodocabeza, int *BO, string infor) {//Función que inserta nodos en el arbol manteniendo el balance
  Nodo *nodo  = NULL;
  Nodo *nodo1 = NULL;
  Nodo *nodo2 = NULL; 
  nodo = *nodocabeza;
  if (nodo != NULL) {
    if (infor < nodo->info) {//Si el valor ingresado es menor al que estamos revisando..
      InsercionBalanceado(&(nodo->izq), BO, infor);//Recursividad hacia la izquierda. 
      if(*BO == TRUE) {
        switch (nodo->FE) {
          case 1: 
            nodo->FE = 0;
            *BO = FALSE;
            break;
          case 0: 
            nodo->FE = -1;
            break;
          case -1: 
            //Reestructuración del árbol.
            nodo1 = nodo->izq;
            //Rotación II.
            if (nodo1->FE <= 0) { 
              nodo->izq = nodo1->der;
              nodo1->der = nodo;
              nodo->FE = 0;
              nodo = nodo1;
            } else { 
              //Rotación ID.
              nodo2 = nodo1->der;
              nodo->izq = nodo2->der;
              nodo2->der = nodo;
              nodo1->der = nodo2->izq;
              nodo2->izq = nodo1;
              if (nodo2->FE == -1){
                nodo->FE = 1;
              }
              else{
                nodo->FE =0;
              }
              if (nodo2->FE == 1){
                nodo1->FE = -1;
              }
              else{
                nodo1->FE = 0;
              }
              nodo = nodo2;
            }
            nodo->FE = 0;
            *BO = FALSE;
            break;
        }
      } 
    } 
    else {
      if (infor > nodo->info) {//Si el valor ingresado es mayor al que estamos revisando...
        InsercionBalanceado(&(nodo->der), BO, infor);//Recursividad hacia la derecha
        if (*BO == TRUE) {
          switch (nodo->FE) {
            case -1: 
              nodo->FE = 0;
              *BO = FALSE;
              break;
            case 0: 
              nodo->FE = 1;
              break;
            case 1: 
              //Reestructuración del arbol. 
              nodo1 = nodo->der;
              if (nodo1->FE >= 0) { 
                //Rotación DD
                nodo->der = nodo1->izq;
                nodo1->izq = nodo;
                nodo->FE = 0;
                nodo = nodo1;
              } else { 
                //Rotación DI. 
                nodo2 = nodo1->izq;
                nodo->der = nodo2->izq;
                nodo2->izq = nodo;
                nodo1->izq = nodo2->der;
                nodo2->der = nodo1;
                if (nodo2->FE == 1)
                  nodo->FE = -1;
                else
                  nodo->FE = 0;
                if (nodo2->FE == -1)
                  nodo1->FE = 1;
                else
                  nodo1->FE = 0;
                nodo = nodo2;
              }
              nodo->FE = 0;
              BO = FALSE;
              break;
          }
        }
      } 
      else {//Si el nodo se encuentra, se da aviso. 
        cout << "El nodo ya se encuentra en el árbol\n";
      }
    }
  } 
  else {//Si el nodo no se encuentra, se crea. 
    nodo = crearNodo(infor);
    *BO = TRUE;
  }
  *nodocabeza = nodo;
}
//Reestructuración 1: 
void Restructura1(Nodo **nodocabeza, int *BO) {
  Nodo *nodo, *nodo1, *nodo2; 
  nodo = *nodocabeza;
  if (*BO == TRUE) {
    switch (nodo->FE) {
      case -1: 
        nodo->FE = 0;
        break;
      case 0: 
        nodo->FE = 1;
        *BO = FALSE;
        break;
    case 1: 
     //Reestructuración del árbol. 
      nodo1 = nodo->der;
      if (nodo1->FE >= 0) { 
        //Rotación DD.
        nodo->der = nodo1->izq;
        nodo1->izq = nodo;
        switch (nodo1->FE) {
          case 0: 
            nodo->FE = 1;
            nodo1->FE = -1;
            *BO = FALSE;
            break;
          case 1: 
            nodo->FE = 0;
            nodo1->FE = 0;
            *BO = FALSE;
            break;           
        }
        nodo = nodo1;
      } else { 
        //Rotación DI
        nodo2 = nodo1->izq;
        nodo->der = nodo2->izq;
        nodo2->izq = nodo;
        nodo1->izq = nodo2->der;
        nodo2->der = nodo1;
       
        if (nodo2->FE == 1)
          nodo->FE = -1;
        else
          nodo->FE = 0;
        
        if (nodo2->FE == -1)
          nodo1->FE = 1;
        else
          nodo1->FE = 0;
       
        nodo = nodo2;
        nodo2->FE = 0;       
      } 
      break;   
    }
  }
  *nodocabeza=nodo;
}
//Reestructuración 2. 
void Restructura2(Nodo **nodocabeza, int *BO) {
  Nodo *nodo, *nodo1, *nodo2; 
  nodo = *nodocabeza;
  if (*BO == TRUE) {
    switch (nodo->FE) {
      case 1: 
        nodo->FE = 0;
        break;
      case 0: 
        nodo->FE = -1;
        *BO = FALSE;
        break;
      case -1: 
        //Reestructuracion del árbol
        nodo1 = nodo->izq;
        if (nodo1->FE<=0) { 
          //Rotacion II
          nodo->izq = nodo1->der;
          nodo1->der = nodo;
          switch (nodo1->FE) {
            case 0: 
              nodo->FE = -1;
              nodo1->FE = 1;
              *BO = FALSE;
              break;
            case -1: 
              nodo->FE = 0;
              nodo1->FE = 0;
              *BO = FALSE;
              break;
          }
          nodo = nodo1;
        } else { 
          //Rotacion ID
          nodo2 = nodo1->der;
          nodo->izq = nodo2->der;
          nodo2->der = nodo;
          nodo1->der = nodo2->izq;
          nodo2->izq = nodo1;
       
          if (nodo2->FE == -1)
            nodo->FE = 1;
          else
            nodo->FE = 0;
        
          if (nodo2->FE == 1)
            nodo1->FE = -1;
          else
            nodo1->FE = 0;
       
          nodo = nodo2;
          nodo2->FE = 0;       
        }      
        break;   
    }
  }
  *nodocabeza = nodo;
}
//Función que borra un nodo. 
void Borra(Nodo **aux1, Nodo **otro1, int *BO) {
  Nodo *aux, *otro; 
  aux=*aux1;
  otro=*otro1; 
  if (aux->der != NULL) {
    Borra(&(aux->der),&otro,BO); 
    Restructura2(&aux,BO);
  } else {
    otro->info = aux->info;
    aux = aux->izq;
    *BO = TRUE;
  }
  *aux1=aux;
  *otro1=otro;
}
//Función que elimina un nodo respetando el balance del árbol. Llama a las 2 reestructura
//ciones y a borrar. 
void EliminacionBalanceado(Nodo **nodocabeza, int *BO, string infor) {
  Nodo *nodo, *otro; 
  nodo = *nodocabeza;
  if (nodo != NULL) {
    if (infor < nodo->info) {
      EliminacionBalanceado(&(nodo->izq),BO,infor);
      Restructura1(&nodo,BO);
    } else {
      if (infor > nodo->info) {
        EliminacionBalanceado(&(nodo->der),BO,infor);
        Restructura2(&nodo,BO); 
      } else {
        otro = nodo;
        if (otro->der == NULL) {
          nodo = otro->izq;
          *BO = TRUE;     
        } else {
          if (otro->izq==NULL) {
            nodo=otro->der;
            *BO=TRUE;     
          } else {
            Borra(&(otro->izq),&otro,BO);
            Restructura1(&nodo,BO);
            delete(otro);
          }
        }
      }
    } 
  } else {
    printf("El nodo NO se encuentra en el árbol\n");
  }
  *nodocabeza=nodo;
}

//Manú principal (texto) retorna la respuesta del usuario. 
int Menu() {
  int Op;
  do {
    cout << "\n--------------------\n";
    cout << "1) Insertar\n";
    cout << "2) Eliminacion\n";
    cout << "3) Reemplazar valores\n";
    cout << "4) Imprimir (inorden)\n";
    cout << "5) Grafo\n";
    cout << "0) Salir\n\n";
    cout << "Opción: ";
    cin >> Op;
  } while (Op<0 || Op>7);
  return (Op);
}

//Función main. 
int main() {
  int cant_ids;//Hay un problema al utilizar todos los ids (son demasiados y no genera nunca el grafo)
  //Decidí pedir una cantidad de ids por archivo, para demostrar que se pueden abrir todos y que se genera el grafo correspondiente. 
  cout << "Ingrese número de ids a utilizar (por archivo): " << endl;
  cin >> cant_ids;
  ifstream archivo;//Stream en el que se lee el archivo
  string id;
  Nodo *raiz = NULL;
  int opcion;
  string elemento;
  int inicio;
  int cont = 0;
  ////Se abre el archivo 1 y se van agregando los datos desde los archivos. 
  archivo.open("data.txt", ios::in);
  while(!archivo.eof() && cont <= cant_ids){//Ciclo para agregar nodos. 
    getline(archivo, id);
    inicio = FALSE;
    InsercionBalanceado(&raiz, &inicio, id);
    cont++;
  }
  //
  opcion = Menu();

  //Ciclo para desplegar las ociones del menu
  while (opcion != 0) {
    //Dependiendo de la opción funcionará cada función
    switch(opcion) {
      case 1:
        cout << "Ingresar elemento: " << endl;
        cin >> elemento;
        inicio = FALSE;
        InsercionBalanceado(&raiz, &inicio, elemento);
        break;
      case 2:
        cout << "Elemento a borrar" << endl;
        cin >> elemento;
        inicio = FALSE;
        EliminacionBalanceado(&raiz, &inicio, elemento);
        break;
      case 3:
        cout << "Elemento antiguo: " << endl;
        cin >> elemento;
        EliminacionBalanceado(&raiz, &inicio, elemento);
        cout << "Elemento nuevo: " << endl;
        cin >> elemento;
        inicio = FALSE;
        InsercionBalanceado(&raiz, &inicio, elemento);
        break;
      case 4:
        inorden(raiz);
        break;
      case 5:
        Grafo *grafo = new Grafo(raiz);
        break;
    }
    opcion = Menu();
  }
}