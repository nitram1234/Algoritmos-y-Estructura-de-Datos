#include <iostream>
using namespace std;
#define nulo 0

// Estructura de Nodo
typedef struct _Nodo {
    int numero;
    struct _Nodo *sig;
} Nodo;

// Hash principal
// Ordena los datos ingresados respecto del resto asignándoles un módulo
int hashModulo(int numero, int primo){
    int clave;
    clave = numero % primo;
    return clave;
}

// Método Doble Hash
int hash2(int numero, int primo){
    int clave = (numero+2)%primo;
    return clave;
}

// Ciclo para imprimir el arreglo ingresado por el usuario
void imprimirArreglo(int* arreglo, int tamano){
    for(int i=0; i<tamano; i++){
        if(arreglo[i] == nulo){
            cout << "- ";
        }
        else{
            cout << arreglo[i] << " ";
        }
    }
}

// Ciclo para imprimir el arreglo ordenado por el método ingresado por el usuario
void imprimirArregloLista(Nodo* arreglo, int tamano){
    for(int i=0; i<tamano; i++){
        if(arreglo[i].numero == nulo){
            cout << "-";
        }
        else{
            cout << arreglo[i].numero << " ";
        }
        if(arreglo[i].sig != nullptr){
            Nodo* aux;
            aux = arreglo[i].sig;
            while(aux != nullptr){
                cout << "-> " << aux->numero << " ";
                aux = aux->sig;
            }
        }
        cout << endl;
    }
}

// Método de Reasignación Lineal
void reasignacionLineal(int numero, int* arreglo, int tamano, int primo){
    if(arreglo[hashModulo(numero, primo)] == nulo){ // En caso de que no haya colisión
       arreglo[hashModulo(numero, primo)] = numero; // el valor se agrega al arreglo.
    }
    else{                                   // En caso contrario
        int n = hashModulo(numero, primo);
        while(arreglo[n%tamano] != nulo){   // el ciclo recorre el arreglo hasta encontrar un espacio vacío
            n++;
        }
        arreglo[n%tamano] = numero;         // y agrega el valor ahí
    }
}

// Método de Reasignación Cuadrática
void reasignacionCuadratica(int numero, int* arreglo, int tamano, int primo){
    if(arreglo[hashModulo(numero, primo)] == nulo){ // Mismo comentario que en la Reasignacion Lineal
       arreglo[hashModulo(numero, primo)] = numero;
    }
    else{
        int i = 1;
        int n = hashModulo(numero, primo);
        while(arreglo[(n + i*i)%tamano]!= nulo){    // Este while busca espacios vacíos en el arreglo,
            i++;
            if(i*i > 46000){                        // pero saltándose espacios cuadráticamente
                cout << "No se puede guardar el dato, i² excede el valor posible de un número int. " << endl;
                break;
            }
        }
        arreglo[(n + i*i)%tamano] = numero;         // En caso de que el espacio vacío encontrado esté dentro del arreglo, le asigna el valor ingresado.
    }
}

// Método de Reasignación Doble Hash
void reasignacionDobleHash(int numero, int* arreglo, int tamano, int primo){
    int D = hashModulo(numero, primo);
    if(arreglo[D] == nulo){
       arreglo[D] = numero;
    }
    else{
        int DX = hash2(D, primo); //llama a la función hash2 para buscar un espacio vacío y sin colisión al valor
        while(arreglo[DX] != nulo){
            DX = hash2(DX, primo);
        }
        arreglo[DX] = numero;
    }
}

// Método de Encadenamiento
void encadenamiento(int numero, Nodo* arreglo, int tamano, int primo){
    if(arreglo[hashModulo(numero, primo)].numero == nulo){ // Cada valor ingresado es asignado al primer espacio sin valor asignado.
       arreglo[hashModulo(numero, primo)].numero = numero;
    }
    else{
        Nodo* nodo = new Nodo;
        nodo->numero = numero;
        nodo->sig = nullptr;
        if(arreglo[hashModulo(numero, primo)].sig == nullptr){  //si la posición no apunta a algún nodo
            arreglo[hashModulo(numero, primo)].sig = nodo;  //se agrega un nodo a .sig
        }
        else{                                               //en caso contrario se recorre el arreglo hasta encontrar un .sig vacío
            Nodo* aux = arreglo[hashModulo(numero, primo)].sig;
            while(aux->sig != nullptr){
                aux = aux->sig;
            }
            aux->sig = nodo;                // y se ingresa el nodo a este.
        }
    }
}

// Métodos de Búsqueda según el método de reasignación:


void busquedaLineal(int numero, int* arreglo, int tamano, int primo){
    int D = hashModulo(numero, primo);
    int DX;
    if(arreglo[D] != nulo && arreglo[D] == numero){
        cout << "La información está en la posición: " << D;
    }
    else{
        DX = D + 1;
        while(DX < tamano && arreglo[DX] != nulo && arreglo[DX] != numero && DX != D){
            DX = DX + 1;
            if(DX == tamano){
                DX = 0;
            }
        }
        if(arreglo[DX] == nulo || DX == D){
            cout << "La información no se encuentra en el arreglo" << endl; 
        }
        else{
            cout << "La información está en la posición: " << DX << endl;
        }
    }
}
void busquedaCuadratica(int numero, int* arreglo, int tamano, int primo){
    int D = hashModulo(numero, primo);
    int DX;
    int I;
    if(arreglo[D] != nulo && arreglo[D] == numero){
        cout << "La información está en la posición: " <<  D << endl; 
    }
    else{
        I = 1;
        DX = D + I*I;
        while(arreglo[DX] != nulo && arreglo[DX] != numero){
            I = I + 1;
            DX = D + I*I;
        }
        if(DX >= tamano){
            I = 0;
            DX = 0;
            D = 0;
        }
        if(arreglo[DX] == nulo){
            cout << "La información no se encuentra en el arreglo" << endl;
        }
        else{
            cout << "La información está en la posición: " << DX; 
        }
    }
}
void busquedaDobleHash(int numero, int* arreglo, int tamano, int primo){
    int D = hashModulo(numero, primo);
    int DX;
    if(arreglo[D] != nulo && arreglo[D] == numero){
        cout << "La información está en la posición " << D << endl;
    }
    else{
        DX = hash2(D, primo);
        while(DX < tamano && arreglo[DX] != nulo && arreglo[DX] != numero && DX != D){
            DX = hash2(DX, primo);
        }
        if(arreglo[DX] != nulo & arreglo[DX] != numero){
            cout << "La información no se encuentra en el arreglo" << endl;
        }
        else{
            cout << "La información está en la posición " << DX; 
        }
    }
}
void busquedaEncadenamiento(int numero, Nodo* arreglo, int tamano, int primo){
    int D = hashModulo(numero, primo);
    Nodo* Q;
    if(arreglo[D].numero != nulo && arreglo[D].numero == numero){
        cout << "La información está en la posición " << D;
    }
    else{
        Q = arreglo[D].sig;
        while(Q != nullptr && Q->numero != numero){
            Q = Q->sig;
        }
        if(Q == nullptr){
            cout << "La información no se encuentra en la lista";
        }
        else{
            cout << "La información está en la lista de la posición " << D;
        }
    }
}
int main(int argc, char *argv[]){
    int tamano = 20;
    int primo = 23;
    char param = argv[1][0];
    if(param != 'L' && param != 'C' && param != 'D' && param != 'E'){
        cout << "Parámetro no permitido" << endl;
        exit(-1);
    }
    int arreglo[tamano] = {nulo};
    Nodo arregloLista[tamano];
    for(int i=0; i<tamano; i++){
        arregloLista[i].numero = nulo;
        arregloLista[i].sig = nullptr;
    }
    for(int i=0; i<tamano; i++){
        int numero;
        cout << "Ingresa valor " << i+1 << ": " << endl;
        cin >> numero;
        if(param == 'L'){
            reasignacionLineal(numero, arreglo, tamano, 19);
        }
        if(param == 'C'){
            reasignacionCuadratica(numero, arreglo, tamano, 19);
        }
        if(param == 'D'){
            reasignacionDobleHash(numero, arreglo, tamano, primo);
        }
        if(param == 'E'){
            encadenamiento(numero, arregloLista, tamano, 23);
        }
    }
    while(1){
        string res;
        cout << endl << "--------------------" << endl;
        cout << "|[1] Imprimir datos|" << endl;
        cout << "|[2] Buscar dato   |" << endl;
        cout << "------------------" << endl;
        cin >> res;
        if(res == "1"){
            if(param == 'E'){
                imprimirArregloLista(arregloLista, tamano);
            }
            else{
                imprimirArreglo(arreglo, tamano);
            }
        }
        if(res == "2"){
            cout << "Ingrese valor: ";
            cin >> res;
            int n = stoi(res);
            if(param == 'L'){
                busquedaLineal(n, arreglo, tamano, primo);
            }
            if(param == 'C'){
                busquedaCuadratica(n, arreglo, tamano, 19);
            }
            if(param == 'D'){
                busquedaDobleHash(n, arreglo, tamano, 19);
            }
            if(param == 'E'){
                busquedaEncadenamiento(n, arregloLista, tamano, 23);
            }
        }
    }   
}