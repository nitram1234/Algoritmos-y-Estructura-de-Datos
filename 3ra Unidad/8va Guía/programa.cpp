#include <iostream>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <chrono>
#include "Seleccion.h"
#include "Quicksort.h"
using namespace std;
void imprimirVector(int *vector, int elementos){
    for (int i = 0; i < elementos; i++){
        cout << vector[i] << " ";
    }
    cout << endl;
}
int main(int argc, char *argv[]){
    using namespace std::chrono;
    bool ver;
    cout << "¿Ver? (1: Sí, 0: No)" << endl;
    cin >> ver;
    int elementos = stoi(argv[1]);
    int tamano_max = 1000000;
    int vector[tamano_max];
    srand((unsigned) time(0));
    for (int i = 0; i < elementos; i++) {
        vector[i] = (rand() % 30) + 1;
    }
    Seleccion seleccion;
    Quicksort quicksort;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    int *vectorSeleccion = seleccion.Ordenar(vector, elementos);
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    int *vectorQuicksort = quicksort.Ordenar(vector, elementos);
    high_resolution_clock::time_point t3 = high_resolution_clock::now();
    cout << "Seleccion:" << endl;
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    if(ver == 1) imprimirVector(vectorSeleccion, elementos);
    cout << "Tiempo: " << time_span.count() << endl;
    cout << "Quicksort: " << endl;
    time_span = duration_cast<duration<double>>(t3 - t2);
    if(ver == 1) imprimirVector(vectorSeleccion, elementos);
    cout << "Tiempo: " << time_span.count() << endl;
    return 0;
}